'use strict';

var expect = require('chai').expect;
var utils = require('../utils');

describe('addInterestsScores', function(){
  it('should add the corrent scores', function(){
    var user = {interests: ['a', 'b']};
    var users = [
      {interests: ['a', 'c']},
      {interests: ['a', 'b', 'c']},
      {interests: []}
    ];
    var expectedResults = [
      {interests: ['a', 'c'], interestsScore: 1},
      {interests: ['a', 'b', 'c'], interestsScore: 2},
      {interests: [], interestsScore: 0}
    ];
    var results = utils.addInterestsScores(user, users);
    expect(results).to.deep.equal(expectedResults);
  });
});

describe('filterIgnored', function(){
  it('should ignore users', function(){
    var user = {ignored: ['a', 'b']};
    var users = [
      {username: 'a'},
      {username: 'c'},
      {username: 'b'}
    ];
    var expectedResults = [
      {username: 'c'}
    ];
    var results = utils.filterIgnored(user, users);
    expect(results).to.deep.equal(expectedResults);
  });
});

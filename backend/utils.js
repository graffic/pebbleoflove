'use strict';

var fs = require('fs');
var path = require('path');
var _ = require('lodash');

exports.addInterestsScores = function(user, users) {
  var userInterests = user.interests;
  var results = [];
  _.forEach(users, function(u) {
    var count = _.intersection(userInterests, u.interests).length;
    u.interestsScore = count;
    results.push(u);
  });
  return results;
};

exports.filterIgnored = function(user, users) {
  var ignored = user.ignored;
  return _.filter(users, function(u) {
    return _.indexOf(ignored, u.username) === -1;
  });
};

exports.initialUserData = function() {
  var filePath = path.join(__dirname, 'initialUserData.json');
  var data = fs.readFileSync(filePath, {encoding: 'utf8'});
  return JSON.parse(data);
};

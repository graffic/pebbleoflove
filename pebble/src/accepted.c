#include <pebble.h>

typedef struct {
    Window *window;
    TextLayer *display_layer;
    BitmapLayer *logo_layer;

    GBitmap *logo_bitmap;
    char display[100];
} AcceptedUi;


static void load_window(Window *window) {
    Layer *layer;
    Layer *window_layer = window_get_root_layer(window);
    AcceptedUi *ui = window_get_user_data(window);

    // Message
    ui->display_layer = text_layer_create((GRect) {
        .origin = {0, 0},
        .size = {144,70}
    });
    text_layer_set_text_alignment(ui->display_layer, GTextAlignmentCenter);
    text_layer_set_text(ui->display_layer, ui->display);
    text_layer_set_font(ui->display_layer,
        fonts_get_system_font(FONT_KEY_DROID_SERIF_28_BOLD));
    layer = text_layer_get_layer(ui->display_layer);
    layer_add_child(window_layer, layer);

    // Logo
    ui->logo_layer = bitmap_layer_create((GRect) {
        .origin = {26, 65},
        .size = {89, 86}
    });
    bitmap_layer_set_bitmap(ui->logo_layer, ui->logo_bitmap);
    layer = bitmap_layer_get_layer(ui->logo_layer);
    layer_add_child(window_layer, layer);
}

static void unload_window(Window *window) {
    AcceptedUi *ui = window_get_user_data(window);

    text_layer_destroy(ui->display_layer);
    bitmap_layer_destroy(ui->logo_layer);
    gbitmap_destroy(ui->logo_bitmap);

    free(ui);
    window_destroy(window);
}

void create_accepted_window(char *name) {
    AcceptedUi *ui = malloc(sizeof(AcceptedUi));

    ui->window = window_create();
    window_set_user_data(ui->window, ui);
    window_set_window_handlers(ui->window, (WindowHandlers) {
        .load = load_window,
        .unload = unload_window
    });
    ui->logo_bitmap = gbitmap_create_with_resource(
        RESOURCE_ID_ACCEPTED_LOGO);

    snprintf(ui->display, 100, "%s accepted!", name);

    window_stack_push(ui->window, true);
}
